This project is to design a relay board with raspberry pi HAT board.
The project contains 2 phase.

1. Raspberry Pi UPS HAT board

- When the external power is lost, the UPS HAT board has to keep the Pi powered.
- The UPS HAT board should contains a battery status IC that can report the battery charge level to the Pi. If this level decreases to a certain low value, the Pi may decide to execute a shutdown.
- The board has to detect whether the Pi has shut down. It has to continue to provide power to the Pi until the external power supply get back. And in case the external power returns, the Pi hast to turn on.
- When the external power is applied, the Pi has to automatically switched on. 
- When external power is lost and returns, the board will have to resume charging the battery. 

2. Relay mother board.

- board has to fit Standard DIN rail mounting enclosure
- 12v/24v power supply with surge and reverse polarity protection, 
- External battery connector,
- On-board LED for battery operations status,
- File Safe Shutdown Functionality,
- On-board LEDs for power supply (24v, 12v, 5v, 3V3) and serial line activity,
- Lcd connector (16x2),
- external ADC (Analog Digital Converter) connected via SPI to Raspberry Pi,
- Real time clock (RTC) with on-board lithium back-up battery,
- PWM FAN control,
- Buzzer,
- 8 Relays + leds (8-Channel Relay Module with Optocoupler H/L Level Triger 12V/5V ?,  relays that can be configured as ACTIVE-HI and ACTIVE-LOW)
- I2C Eeprom  
- Reset Button
- 40 pins female connector to interface to the Rasperry Pi 3,
- Power supply, connections ( serial, can...), digital and analog I/O on screw terminal block

- Headers connectors:

(Standard RS-232 and RS-485 interfaces to the Raspberry Pi serial line, with opto-isolator and electrostatic discharge protection)

	* 1 x Protected RS232 Port 
	* 1 x Protected RS485 Port 
	* 1 x Protected 1-wire port
	* 1 x Protected I2C port
	* 1 x Protected SPI port
	* 1x Protected CAN bus to connect additional board 
	- Stackable Header for breadboard
	
reference site: https://brousant.nl/jm3/index.php/elektronica/104-geekworm-ups-for-raspberry-pi


		